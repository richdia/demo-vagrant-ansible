# Démo Vagant Ansible

## Installation

[![N|Solid](http://icons.iconarchive.com/icons/dakirby309/simply-styled/256/VirtualBox-icon.png)](https://download.virtualbox.org/virtualbox/5.1.26/VirtualBox-5.1.26-117224-Win.exe)[![N|Solid](https://dl2.macupdate.com/images/icons256/49997.png?d=1525980457)](https://releases.hashicorp.com/vagrant/2.1.1/vagrant_2.1.1_x86_64.msi)

Pour le bon fonctionnement il faut exécuter le script Bash `init.sh`

## Instructions
* Lancez un invite de commande (Ex : PowerShell, Git Bash, Etc.)
  * Recommendé en mode administrateur
* Dirigez vous vers l'emplacement  du projet

## Commandes
* La commande pour lancer la VM est `vagrant up`
* La commnade pour arrêter la VM est `vagrant halt`
* La commande pour se connecter à la VM est `vagrant ssh`
* La commande pour supprimer la VM est `vagrant destroy`

##### Attention
La commande `ssh` va obligatoirement demander le nom de la  machine à se connecter.

Les noms disponibles sont :
* `default`
* `v1`
* `v2`

Exemple : `vagrant ssh v1`
